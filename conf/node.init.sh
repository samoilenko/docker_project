#!/bin/bash

mkdir -p /tmp/modules
cp /project/package.json /tmp/modules/package.json
cd /tmp/modules
npm install
mv /tmp/modules/node_modules /project/node_modules
cd /project

rm /tmp/modules/package.json

node app.js
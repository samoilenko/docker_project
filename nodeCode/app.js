/**
 * Created by s.samoilenko@gmail.com on 05.04.2016.
 */

var http = require('http');

const server = http.createServer((req,res) => {
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('X-Foo', 'bar');
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('ok');
});

server.listen(8000);

